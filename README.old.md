# React-Router-Redux
- React Router;
- Redux;
- Redux thunks.

## Цели занятия
- добавить многостраничность с React Router;
- добавить работу с стейтом через Redux.


## Краткое содержание
- React Router;
- Redux;
- Redux thunks.


## Дата и время
- 6 июля, среда в 20:00
- Длительность занятия: 90 минут
